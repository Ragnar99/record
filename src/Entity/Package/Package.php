<?php


namespace App\Entity\Package;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Package\PackageRepository")
 * @ORM\Table(name="package")
 */
class Package
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     *
     */
    private $articleId;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     *
     */
    private $name;

    /**
     * @var integer
     * @ORM\Column(type="integer", nullable=true)
     */
    private $count;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $dateOfReceipt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $dateOfExpiry;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Market\Market", inversedBy="packages")
     */
    private $market;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getArticleId(): ?string
    {
        return $this->articleId;
    }

    /**
     * @param string $articleId
     */
    public function setArticleId(string $articleId): void
    {
        $this->articleId = $articleId;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return int|null
     */
    public function getCount(): ?int
    {
        return $this->count;
    }

    /**
     * @param int $count
     */
    public function setCount(int $count): void
    {
        $this->count = $count;
    }

    /**
     * @return \DateTime|null
     */
    public function getDateOfReceipt(): ?\DateTime
    {
        return $this->dateOfReceipt;
    }

    /**
     * @param \DateTime $dateOfReceipt
     */
    public function setDateOfReceipt(\DateTime $dateOfReceipt): void
    {
        $this->dateOfReceipt = $dateOfReceipt;
    }

    /**
     * @return \DateTime|null
     */
    public function getDateOfExpiry(): ?\DateTime
    {
        return $this->dateOfExpiry;
    }

    /**
     * @param \DateTime $dateOfExpiry
     */
    public function setDateOfExpiry(\DateTime $dateOfExpiry): void
    {
        $this->dateOfExpiry = $dateOfExpiry;
    }

    /**
     * @return mixed
     */
    public function getMarket()
    {
        return $this->market;
    }

    /**
     * @param mixed $market
     */
    public function setMarket($market): void
    {
        $this->market = $market;
    }

}