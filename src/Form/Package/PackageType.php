<?php


namespace App\Form\Package;


use App\Entity\Package\Package;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PackageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('articleId', TextType::class, [
                'required' => false,
                'label' => 'Sifra artikla',
            ])
            ->add('name', TextType::class, [
                'label' => 'Naziv artikla',
                'required' => false,
            ])
            ->add('count', NumberType::class, [
                'label' => 'Broj artikala u posiljci',
                'required' => false,
            ])
            ->add('dateOfReceipt', DateTimeType::class, [
                'label' => 'Datum zaprimanja paketa',
                'required' => false,
            ])
            ->add('dateOfExpiry', DateTimeType::class, [
                'label' => 'Datum isteka roka',
                'required' => false,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults([
            'data_class' => Package::class,
        ]);

    }

    public function getBlockPrefix()
    {
        return 'package_type';
    }

}