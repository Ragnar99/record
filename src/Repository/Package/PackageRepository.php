<?php


namespace App\Repository\Package;


use App\Entity\Package\Package;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class PackageRepository extends ServiceEntityRepository
{

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Package::class);
    }

    public function getPackageWhichExpiryIn7Days($now, $day7)
    {
        $qb = $this->createQueryBuilder('package')
            ->select('package')
            ->where('package.dateOfExpiry >= :now')
            ->andWhere('package.dateOfExpiry < :day7')
            ->setParameter('now', $now)
            ->setParameter('day7', $day7)
            ->orderBy('package.dateOfExpiry', 'DESC');
        return $qb->getQuery()->getResult();
    }

    public function getPackageWhichExpiryIn15Days($day7, $day15)
    {
        $qb = $this->createQueryBuilder('package')
            ->select('package')
            //->where('package.dateOfExpiry >= :day7')
            //->andWhere('package.dateOfExpiry < :day15')
            //->setParameter('day7', $day7)
            //->setParameter('day15', $day15)
            ->orderBy('package.dateOfExpiry', 'DESC');
        return $qb;
    }

    public function getPackages($day15)
    {
        $qb = $this->createQueryBuilder('package')
            ->select('package')
            ->where('package.dateOfExpiry >= :day15')
            ->setParameter('day15', $day15)
            ->orderBy('package.dateOfExpiry', 'DESC');
        return $qb;
    }

}