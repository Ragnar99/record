<?php


namespace App\Controller\Site;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class DashboardController extends AbstractController
{
    public function dashboard(Request $request)
    {
        return $this->render('site/dashboard.html.twig', [
        ]);
    }
}