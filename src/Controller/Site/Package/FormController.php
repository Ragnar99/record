<?php


namespace App\Controller\Site\Package;


use App\Entity\Package\Package;
use App\Form\Package\PackageType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class FormController extends AbstractController
{

    public function form(Request $request)
    {
        $package = new Package();

        $form = $this->createForm(PackageType::class, $package);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $em = $this->getDoctrine()->getManager();
                $em->persist($package);
                $em->flush();

                $this->addFlash('success', 'Paket uspjesno dadat!');

            }catch (\Exception $exception) {

                $this->addFlash('error', 'flash.error.body');

            }

            return $this->redirectToRoute('package_create');
        }

        return $this->render('site/package/form.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}