<?php


namespace App\Controller\Site\Package;


use App\Entity\Package\Package;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ListExpiryIn15DaysController extends AbstractController
{
    public function list()
    {
        $em = $this->getDoctrine()->getManager();

        $day7 = new \DateTime();
        $day7->modify('+7 days');
        $day15 = new \DateTime();
        $day15->modify('+15 days');

        $packages = $em->getRepository(Package::class)->getPackageWhichExpiryIn15Days($day7, $day15);

        return $this->render('site/package/list15.html.twig', [
            'packages' => $packages
        ]);
    }
}