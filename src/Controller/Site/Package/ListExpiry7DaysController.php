<?php


namespace App\Controller\Site\Package;


use App\Entity\Package\Package;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ListExpiry7DaysController extends AbstractController
{
    public function list()
    {
        $em = $this->getDoctrine()->getManager();

        $now = new \DateTime();
        $day7 = new \DateTime();
        $day7->modify('+7 days');


        $packages = $em->getRepository(Package::class)->getPackageWhichExpiryIn7Days($now, $day7);

        return $this->render('site/package/list7.html.twig', [
            'packages' => $packages
        ]);
    }
}