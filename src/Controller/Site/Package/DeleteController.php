<?php


namespace App\Controller\Site\Package;


use App\Entity\Package\Package;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DeleteController extends AbstractController
{
    public function delete($id, $num){

        $em = $this->getDoctrine()->getManager();
        /**
         * @var Package $package
         */
        $package = $this->getDoctrine()->getRepository(Package::class)->findOneBy(['id' => $id]);

        try{

            $em->remove($package);
            $em->flush();
            $this->addFlash('success', 'Paket uspjesno izbrisan.');
        }catch (\Exception $exception){
            $this->addFlash('error', 'An error occurred, please try again.');
        }

        if($num == 7)
        {
            return $this->redirectToRoute('package_list_7');
        }elseif ( $num == 15){
            return $this->redirectToRoute('package_list_15');
        }else{
            return $this->redirectToRoute('package_list_7');
        }

    }
}